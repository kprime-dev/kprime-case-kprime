ID | Description | Type 
|---|---|---|
actor | Two types of actors exist: agents and users. | 
agent | An actor with concrete, physical manifestations, such as
a human individual, an organization, or a department | actor
conceptual model | A conceptual model aims to express the meaning of terms and stories used by domain experts to discuss the problem, and to find the correct relationships between different concepts. The conceptual model is explicitly chosen to be independent of design (logical model) or implementation concerns (physical model). | 
context | A container of goals, stories, and terms referring to a single choesive sub-domain. | 
database | A collection with a name of tables and constraints. | 
dictionary | A semantically, data, provenance enriched collection of terms. | 
goal | A goal is used to model agents relationships, and, eventually, to link organizational needs to system requirements. | schema:address
logical model | A logical data model or logical schema is a data model of a specific problem domain expressed independently of a particular database management product or storage technology (physical data model) but in terms of data structures such as relational tables and columns. May be semantically enriched. | 
physical model | A physical data model (or database design) is a representation of a data design as implemented, or intended to be implemented, in a database management system. In the lifecycle of a project it typically derives from a logical data model, though it may be reverse-engineered from a given database implementation. | 
source | A descriptor to external source of meta data. | 
story | A markdown file with reserved keywords. | schema:text
task | A goal with a story associated. | 
term | A word used to refer to thing in business world. | foaf:#term_Organization
termbase | A potentially data and semantically enriched description of database. | 
trace | A project folder containing termbases, stories, charts. | 
user | A human interacting with kprime system. | actor
vocabulary | A semantically enriched definition of terms. | 