# Story collect and share stories
+ goal 1613676719318
+ actor business
+ assegnee Nicola

---

To collect stories we use agile methodology and practices.
To write stories we choose the best fit for our purpose.



## References

[User stories](https://www.atlassian.com/agile/project-management/user-stories)
[Orchestrating Experiences](https://rosenfeldmedia.com/books/orchestrating-experiences/)
[EventStorming](https://www.eventstorming.com/)
[Domain Storytelling: A Collaborative, Visual, and Agile Way to Build Domain-Driven Software](https://domainstorytelling.org/)
[Patterns for Effective Use Cases](https://dl.acm.org/doi/book/10.5555/556903)




## Other Contexts

[KPrime Domain Driven Design](/project/ddd/cases)


