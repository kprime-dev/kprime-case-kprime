# Story maintain a conceptual model
+ goal 1613676640401
+ assignee Nicola

+ actor modeller
+ command give maintain command 
+ go 2 until changeset complete
+ actor modeller
+ command apply-changeset
+ event model changed
+ source model

Whenever you use a term pay attention.

---
## References 

[Fact Based Modeling Metamodel](http://www.factbasedmodeling.org/Data/Sites/1/media/FBM1002WD08.pdf)

[gUFO: A Lightweight Implementation of the Unified Foundational Ontology](https://nemo-ufes.github.io/gufo/)

[The GMD Data Model and Algebra
for Multidimensional Information](http://www.inf.unibz.it/~franconi/papers/caise-04.pdf)

[Foundations of TemporalConceptual Data Models](http://www.inf.unibz.it/~franconi/papers/Artale-Franconi-chapter-09-springer-preview.pdf)

