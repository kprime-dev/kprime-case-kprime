# Story maintain guiding goals
+ goal 1613676704669
+ actor executive
+ assignee Nicola

---
## References

[Kprime REF](/project/ref/cases)

[REF](https://www.researchgate.net/publication/220535947_Improving_Requirements_Engineering_by_Quality_Modelling_-_A_Quality-Based_Requirements_Engineering_Framework)

[KPrime Tropos](/project/tropos/cases)

[Tropos](http://www.troposproject.eu/)

[iStar](https://sites.google.com/site/istarlanguage/home)

[OKR](https://www.agileway.it/okr-raggiungi-tuoi-obiettivi/)

